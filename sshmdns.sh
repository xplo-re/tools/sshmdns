#!/bin/bash
# -*- Mode: Shell Script; tab-width: 4; indent-tabs-mode: on; coding: utf-8 -*-
# Automated mDNS via SSH Tunnelling
#
# Copyright(C) 2014, xplo.re IT Services, Michael Maier.
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Numeric port pool lower bound to select ports from for proxy forwarding.
# Per default, take script process ID, ensure port >1000 and maxed at 65560.
declare -i PORT=$((1001 + ($$ % 55560)))
# Name of tool, per default equal to script filename.
declare -r SELF="`basename $0`"

# Tool configuration arrays. Path to tool is always put at index 0,
# followed by an arbitrary number of options that are used on each tool
# invocation.
declare -a DIG=('dig' '+time=2' '+noall' '+answer' '+short')
declare -a DNSSD=('dns-sd')
declare -a GREP=('grep')
declare -a LOGGER=('logger' '-i' '-t' "$SELF")
declare -a NETSTAT=('netstat')
declare -a SSH=('ssh' '-oLogLevel=Error' '-oConnectTimeout=2') # '-oLogLevel=Debug')
declare -a WCL=('wc' '-l')

# Storage directory of share definitions.
case "`uname -s`" in
	Darwin)		REPOSITORY="$HOME/Library/Application Support/org.xplo-re.$SELF/Profiles";;
	*)			REPOSITORY="$HOME/.$SELF";;
esac

# INTERNALS ------------------------------------------------------------------
set -e
# set -x
shopt -s nullglob

# Array to store spawned subprocess IDs.
declare -a PROCESSES
# Array of services per server (set as needed).
declare -a SERVICES
# Debug level.
declare -i DEBUG=0
# Verbosity level.
declare -i VERBOSE=0

# Colour codes for message conditions.
CRST=$'\033[0m'
CACK=$'\033[40;32;1m'
CDBG=$'\033[40;37m'
CERR=$'\033[40;31;1m'

# Shortcut for debug message output.
debug()
{
	if test $DEBUG -gt 0;
	then
		if test -z "$IDENT";
		then
			log debug "$1"
		else
			log debug "$IDENT: $1"
		fi
	fi
}

# Shortcut for error message output.
error()
{
	if test -z "$IDENT";
	then
		log error "$1"
	else
		log error "$IDENT: $1"
	fi
}

# Loads a server from repository.
load()
{
	local server="$REPOSITORY/$1.server"

	unset HOST
	unset NAME
	unset INFO
	unset SERVICES

	if test ! -r "$server";
	then
		error "no server description found for '$1'"
		return 0
	else
		source "$server"

		# Return value is existence of host and name.
		return `test ! -z "$HOST" -a ! -z "$NAME"; echo $$?`
	fi
}

# Outputs a log entry both to syslog and stderr.
# $1  Class of log entry, e.g. error, warning, notice.
# $2  Message itself.
log()
{
	local color
	local reset

	# Print to standard out. The system logger is also capable of printing the
	# message out to stderr, but as we want to use colouring, we do this
	# ourselves.
	case $1 in
		debug)	color="${CDBG}"; reset="${CRST}";;
		error)	color="${CERR}"; reset="${CRST}";;
	esac

	echo "${color}`timestamp` $1: $2${reset}" >&2

	# Log to system log.
	"${LOGGER[@]}" -p "user.$1" "$2"
}

# Increments global PORT until the next free port is found.
nextport()
{
	# Always increment port by at least one: If multiple services are initialised,
	# the port might not yet be in use and consequently reported as free.
	PORT=$((PORT+1))

	while test 0 -ne `portusage $PORT`;
	do
		if test $PORT -eq 65560;
		then
			PORT=1000
		fi

		PORT=$((PORT+1))
	done
}

# Yields the number of port usages for a given port.
# $1  Port number to check.
portusage()
{
	"${NETSTAT[@]}" -an | "${GREP[@]}" $1 | "${GREP[@]}" -i listen | "${WCL[@]}"
}

# Publishes all services for the currently loaded server.
publish()
{
	# Iterate through services of current HOST.
	# Yields SERVICE name to forward.
	#        DNSTEXT from remote configuration.
	#        SERVICEPORT from remote configuration.
	for SERVICE in "${SERVICES[@]}";
	do
		debug "setting up service '`servicename`':"
		debug "  retrieving configuration..."

		# Get service port.
		SERVICEPORT="`remotecommand "${DIG[@]} @224.0.0.251 -p 5353 $NAME.$SERVICE.local SRV"`"
		retval=$?

		if test $retval -ne 0;
		then
			debug "  skipping service due to errors getting service port"
			continue
		fi

		SERVICEPORT=($SERVICEPORT)
		SERVICEPORT=${SERVICEPORT[2]}

		# Get service TXT entries.
		rawtext="`remotecommand "${DIG[@]} @224.0.0.251 -p 5353 $NAME.$SERVICE.local TXT"`"
		retval=$?

		if test $retval -ne 0;
		then
			debug "  skipping service due to errors getting service description"
			continue
		fi

		eval "declare -a DNSTEXT=($rawtext)"

		if test -z "$SERVICEPORT" -o "0" = "$SERVICEPORT";
		then
			error "  skipping service due to errors fetching configuration: port invalid"
			continue
		fi

		setup
	done
}

# Executes a command on the current remote HOST.
# $1  Command to execute on remote host.
remotecommand()
{
	local result
	local retval

	result="`"${SSH[@]}" "$HOST" "$1 2>&1" 2>&1`"
	retval=$?

	if test $retval -eq 0;
	then
		echo "$result"
	else
		error "remote command failed for $HOST: $result"
	fi

	return $retval
}

# Prints out list of all services in repository.
servicelist()
{
	local server

	echo "Ident		Description"
	echo "-------------------------------------------------------------------"

	for server in "$REPOSITORY"/*.server;
	do
		server="`basename "$server" .server`"

		load "$server"

		echo "$server	$INFO"
	done
}

# Formats the service name.
servicename()
{
	echo "$NAME.$SERVICE"
}

# Spawns SSH tunnel and mDNS processes.
setup()
{
	local verbosity

	# Set verbosity argument if requested.
	if test $VERBOSE -gt 0;
	then
		verbosity="-v"
	fi

	# Get next free port to use.
	nextport

	debug "  tunnel [$HOST:$SERVICEPORT]->[localhost:$PORT]"

	# SSH tunnel.
	"${SSH[@]}" $verbosity -C -N -L $PORT:localhost:$SERVICEPORT "$HOST" &
	local pidSSH=$!

	# mDNS.
	"${DNSSD[@]}" -P "$NAME" "$SERVICE" local $PORT localhost.local. 127.0.0.1 "${DNSTEXT[@]}" >/dev/null &
	local pidDNS=$!

	debug "  proxy=#$pidSSH, publisher=#$pidDNS"

	PROCESSES=("${PROCESSES[@]}" "$IDENT:$pidSSH" "$IDENT:$pidDNS")
}

# Stops service by killing all spawned processes and exit'ing.
stop()
{
	local -a entry
	local process
	local result

	if test ${#PROCESSES[@]} -gt 0;
	then
		echo
		debug "stopping services..."

		for process in "${PROCESSES[@]}";
		do
			IFS=':' read -a entry <<< "$process"

			# Update ident accordingly for the currently handled process.
			IDENT="${entry[0]}"

			result="`kill "${entry[1]}" 2>&1`"
			retval=$?

			if test $retval -eq 0;
			then
				debug "killed process ${entry[1]}"
			else
				error "failed to kill process ${entry[1]}: $result"
			fi
		done
	fi

	exit $1
}

# Formats timestamp used for on-screen logging.
timestamp()
{
	date '+%Y-%m-%d %H:%M:%S'
}

# Prints usage of tool.
usage()
{
	echo "usage: $SELF [options] [<servers>]"
	echo
	echo "  -d, --debug      Enable debug output."
	echo "  -h, --help       Show this help message."
	echo "  -l, --list       List all existing server definitions in repository."
	echo "  -v, --verbose    Be more verbose (includes invoked tools such as SSH)."
	echo
}

# Automatically invoke stop procedure if script is being killed.
trap stop SIGHUP SIGINT SIGTERM

# Check for server scripts path existence.
if test ! -d "$REPOSITORY";
then
	error "server repository '$REPOSITORY' does not exist"
	exit 1
fi

# Iterate through server names to activate.
# Yields IDENT of server to forward.
#        NAME of server to forward.
#        HOST of server to forward.
#        SERVICES array of service names to forward.
for arg in "$@";
do
	IDENT=

	case "$arg" in
		-d|--debug)
				DEBUG=1
				;;
		-l|--list)
				servicelist
				stop 0
				;;
		-h|--help)
				usage
				stop 0
				;;
		-v|--verbose)
				VERBOSE=$((VERBOSE+1))
				;;
		-*)		error "unknown command '$arg'"
				stop 2
				;;
		*)		load "$arg"
				loaded=$?
				if test $loaded -eq 0;
				then
					IDENT="$arg"
					publish
				fi
				;;
	esac
done

while :;
do
	IDENT=

	echo "Press CTRL-C to stop"
	sleep 86400
done
